//console.log("Hello World");

/*

	3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
	4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

	5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
	6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

	7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

	8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
	9. Update a to do list item by changing the data structure to contain the following properties:
	- Title
	- Description
	- Status
	- Date Completed
	- User ID

	10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
	11. Update a to do list item by changing the status to complete and add a date when the status was changed.
	
	12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

*/

//GET all titles
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => json.map(todos => todos.title))
.then(title => console.log(title));

//GET single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

//Print title and status
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(titleStatus =>  console.log(`The item ${titleStatus.title} on the list has a status of ${titleStatus.completed}`));

//POST
fetch("https://jsonplaceholder.typicode.com/todos",
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: "false",
			title: "Created To Do List Item",
			userId: 201
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));

//PUT
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			status: "pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));

//PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			status: "Complete",
			date: "01/09/2022"
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));

//DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE"
	}
)
.then(response => response.json())
.then(json => console.log(json));